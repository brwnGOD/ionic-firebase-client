import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';

@Component({
  templateUrl: 'app.component.html',
})
export class App {
  rootPage: any = null;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    const config = {
      apiKey: 'AIzaSyCCBYpqefm-uy10xDuJttIJ248fs-9fXhM',
      authDomain: 'records-mash.firebaseapp.com',
      databaseURL: 'https://records-mash.firebaseio.com',
      projectId: 'records-mash',
      storageBucket: 'records-mash.appspot.com',
      messagingSenderId: '628181149523',
    };

    platform.ready().then(() => {
      firebase.initializeApp(config);
      firebase.auth().onAuthStateChanged(data => {
        this.rootPage = firebase.auth().currentUser !== null ? 'RecordsPage' : 'LoginPage';
        statusBar.styleDefault();
        splashScreen.hide();
      });
    });
  }
}
