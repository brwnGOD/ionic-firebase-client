import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FirebaseAuthService } from '../../services/firebase-auth.service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseAuthService: FirebaseAuthService,
    public alertCtrl: AlertController,
  ) {}

  public goToRegistration(): void {
    this.navCtrl.push('RegistrationPage');
  }

  public login({ email, password }): void {
    this.firebaseAuthService.signInWithEmailAndPassword(email, password).subscribe(
      () => {
        this.navCtrl.popToRoot();
      },
      message => {
        this.alertCtrl
          .create({
            title: 'Error',
            subTitle: message,
            buttons: ['OK'],
          })
          .present();
      },
    );
  }
}
