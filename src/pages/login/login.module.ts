import { NgModule } from '@angular/core';
import { IonicPageModule, AlertController } from 'ionic-angular';
import { LoginPage } from './login';
import { LoginFormModule } from '../../components/login-form/login-form.module';
import { FirebaseAuthService } from '../../services/firebase-auth.service';

@NgModule({
  declarations: [LoginPage],
  imports: [IonicPageModule.forChild(LoginPage), LoginFormModule],
  providers: [FirebaseAuthService, AlertController],
})
export class LoginPageModule {}
