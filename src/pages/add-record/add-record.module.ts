import { NgModule } from '@angular/core';
import { IonicPageModule, AlertController } from 'ionic-angular';
import { AddRecordPage } from './add-record';
import { DatabaseService } from '../../services/database.service';
import { AddRecordFormModule } from '../../components/add-record-form/add-record-form.module';

@NgModule({
  declarations: [AddRecordPage],
  imports: [IonicPageModule.forChild(AddRecordPage), AddRecordFormModule],
  providers: [DatabaseService, AlertController],
})
export class AddRecordPageModule {}
