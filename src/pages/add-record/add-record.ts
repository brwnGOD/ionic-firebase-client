import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { DatabaseService } from '../../services/database.service';

@IonicPage({
  defaultHistory: ['RecordsPage'],
})
@Component({
  selector: 'page-add-record',
  templateUrl: 'add-record.html',
})
export class AddRecordPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public database: DatabaseService,
    public alertCtrl: AlertController,
  ) {}

  public add({ name, description }): void {
    this.database.pushRecord({ name, description }).subscribe(null, message => {
      this.alertCtrl
        .create({
          title: 'Error',
          subTitle: message,
          buttons: ['OK'],
        })
        .present();
    });
    this.navCtrl.pop();
  }
}
