import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecordsPage } from './records';
import { DatabaseService } from '../../services/database.service';

@NgModule({
  declarations: [RecordsPage],
  imports: [IonicPageModule.forChild(RecordsPage)],
  providers: [DatabaseService],
})
export class RecordsPageModule {}
