import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseService } from '../../services/database.service';

@IonicPage()
@Component({
  selector: 'page-records',
  templateUrl: 'records.html',
})
export class RecordsPage {
  public data: Array<{
    name: string;
    desctiption: string;
  }> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public database: DatabaseService) {}

  public ionViewDidLoad(): void {
    this.database.getRecords().subscribe(data => {
      const result = [];
      for (const key in data) {
        result.push(data[key]);
      }
      this.data = result;
    });
  }

  public goToAddRecord(): void {
    this.navCtrl.push('AddRecordPage');
  }
}
