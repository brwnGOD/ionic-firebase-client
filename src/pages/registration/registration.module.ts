import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationPage } from './registration';
import { RegistrationFormModule } from '../../components/registration-form/registration-form.module';
import { FirebaseAuthService } from '../../services/firebase-auth.service';

@NgModule({
  declarations: [RegistrationPage],
  imports: [IonicPageModule.forChild(RegistrationPage), RegistrationFormModule],
  providers: [FirebaseAuthService],
})
export class RegistrationPageModule {}
