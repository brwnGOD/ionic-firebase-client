import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FirebaseAuthService } from '../../services/firebase-auth.service';

@IonicPage({
  defaultHistory: ['LoginPage'],
})
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseAuthService: FirebaseAuthService,
    public alertCtrl: AlertController,
  ) {}

  public register({ email, password }): void {
    this.firebaseAuthService.createUserWithEmailAndPassword(email, password).subscribe(
      result => {
        this.navCtrl.popToRoot();
      },
      message => {
        this.alertCtrl
          .create({
            title: 'Error',
            subTitle: message,
            buttons: ['OK'],
          })
          .present();
      },
    );
  }
}
