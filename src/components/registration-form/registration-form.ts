import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'registration-form',
  templateUrl: 'registration-form.html',
})
export class RegistrationFormComponent {
  private registration: FormGroup;
  @Output() onSubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder) {
    this.registration = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      repeatPassword: ['', [Validators.required]],
    });
  }

  public submitForm(): void {
    const { email, password } = this.registration.value;
    this.onSubmit.emit({ email, password });
  }
}
