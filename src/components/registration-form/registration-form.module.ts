import { NgModule } from '@angular/core';
import { RegistrationFormComponent } from './registration-form';
import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [RegistrationFormComponent],
  imports: [IonicModule],
  exports: [RegistrationFormComponent],
})
export class RegistrationFormModule {}
