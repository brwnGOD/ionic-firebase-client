import { NgModule } from '@angular/core';
import { LoginFormComponent } from './login-form';
import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [LoginFormComponent],
  imports: [IonicModule],
  exports: [LoginFormComponent],
})
export class LoginFormModule {}
