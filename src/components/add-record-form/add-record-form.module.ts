import { NgModule } from '@angular/core';
import { AddRecordFormComponent } from './add-record-form';
import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [AddRecordFormComponent],
  imports: [IonicModule],
  exports: [AddRecordFormComponent],
})
export class AddRecordFormModule {}
