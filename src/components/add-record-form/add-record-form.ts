import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'add-record-form',
  templateUrl: 'add-record-form.html',
})
export class AddRecordFormComponent {
  private record: FormGroup;
  @Output() onSubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder) {
    this.record = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: [''],
    });
  }

  public submitForm(): void {
    const { name, description } = this.record.value;
    this.onSubmit.emit({ name, description });
  }
}
