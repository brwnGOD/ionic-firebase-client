import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import firebase from 'firebase';

@Injectable()
export class DatabaseService {
  constructor() {}

  public pushRecord(record: { name: string; description: string }): Observable<any> {
    const key = firebase
      .database()
      .ref()
      .child('records')
      .push().key;

    const updates = {};
    updates['/records/' + key] = record;

    return Observable.create(observer => {
      firebase
        .database()
        .ref()
        .update(updates)
        .then(result => {
          observer.next(result);
        })
        .catch(({ message }) => {
          observer.error(message);
        });
    });
  }

  public getRecords(): Observable<any> {
    return Observable.create(observer => {
      const recordsRef = firebase.database().ref('/records');
      recordsRef.on('value', snapshot => {
        observer.next(snapshot.val());
      });
    });
  }
}
