import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import firebase from 'firebase';

@Injectable()
export class FirebaseAuthService {
  constructor() {}

  public getUser(): Object {
    return firebase.auth().currentUser;
  }

  public signInWithEmailAndPassword(email, password): Observable<any> {
    return Observable.create(observer => {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(result => {
          observer.next(result);
        })
        .catch(({ message }) => {
          observer.error(message);
        });
    });
  }

  public createUserWithEmailAndPassword(email, password): Observable<any> {
    return Observable.create(observer => {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(result => {
          observer.next(result);
        })
        .catch(({ message }) => {
          observer.error(message);
        });
    });
  }
}
